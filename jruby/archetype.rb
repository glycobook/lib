require 'java'

require '/home/glycobook/workspace/libs/java/archetype-0.1.0.jar'
require '/home/glycobook/workspace/libs/java/wurcsframework-1.2.13.jar'
require '/home/glycobook/workspace/libs/java/slf4j-api-2.0.6.jar'

java_import 'org.glytoucan.Archetype'
java_import 'org.glycoinfo.WURCSFramework.wurcs.graph.WURCSGraph'
java_import 'org.glycoinfo.WURCSFramework.util.WURCSException'
java_import 'org.slf4j.Logger'
java_import 'org.slf4j.LoggerFactory'

def archetype(w)
  Archetype.beBorn(w)
end