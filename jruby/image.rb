require 'java'
require 'net/http'
require 'uri'
require 'json'
def showImage(id)
  uri = URI.parse("https://api.glycosmos.org/wurcs2image/latest/png/html/" + id)
  response = Net::HTTP.get_response(uri)
  IRuby.html "<fieldset><legend>" + id + "</legend>" + response.body + "</fieldset>"
end