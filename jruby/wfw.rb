require 'java'
require 'net/http'
require 'uri'
require 'json'

require '/home/glycobook/workspace/libs/java/wurcsframework-1.2.13.jar'
require '/home/glycobook/workspace/libs/java/slf4j-api-2.0.6.jar'

java_import 'org.slf4j.Logger'
java_import 'org.glycoinfo.WURCSFramework.util.validation.WURCSValidator'

def validator(w)
  validator = WURCSValidator.new
  validator.start(w)

  return { "WURCS" => validator.getReport().toString(),
           "ERROR" => validator.getReport().hasError(),
           "WARNING" => validator.getReport().hasWarning(),
           "UNVERIFIABLE" => validator.getReport().hasUnverifiable(),
           "STANDERD" => validator.getReport().standard_string(),
           "RESULTS" => validator.getReport().getResults()
  }
end
